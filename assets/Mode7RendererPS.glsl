#ifdef GL_ES
	precision highp float;
#endif

uniform sampler2D trackTexture;
uniform vec2 screenSize; 
uniform vec2 trackTextureSize;
uniform vec2 windowCenter;
uniform vec2 cameraPosition;
uniform vec2 cameraScreenPosition;
uniform float cameraAngle;
uniform float cameraFov;
uniform float deltaAngle;

void main()
{
	float rayDistance;
	vec2 texel;
	float currentAngle;
	
	rayDistance = (cameraScreenPosition.y * cameraScreenPosition.x) / ((screenSize.y - gl_FragCoord.y - 1.0) - windowCenter.y);
	currentAngle = (-cameraFov / 2.0) + deltaAngle * gl_FragCoord.x;
	texel.x = (cameraPosition.x + rayDistance / cos(currentAngle) * cos(cameraAngle + currentAngle));
	texel.y = (cameraPosition.y + rayDistance / cos(currentAngle) * sin(cameraAngle + currentAngle));
	if (!((screenSize.y - gl_FragCoord.y - 1.0) < windowCenter.y || texel.x < 0.0 || texel.y < 0.0 || texel.x >= trackTextureSize.x || texel.y >= trackTextureSize.y))
	{
		gl_FragColor = texture2D(trackTexture, texel / trackTextureSize);
	}
	else
		discard;
}