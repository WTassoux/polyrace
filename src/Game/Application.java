package Game;

import menuV2.MainScreen;
import screen.MenuScreen;
import screen.Screen;

import AudioEngine.AudioEngine;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import configuration.Configuration;

public class Application implements ApplicationListener {

	private Music introductionMusic;
	private Texture introductionTexture;

	private Screen screen;
	private SpriteBatch batch;
	private Input input;
	private AudioEngine audio;
	
	public Configuration config;

	@Override
	public void create() {
		input = new Input();
		Gdx.input.setInputProcessor(input);
		screen = new MainScreen(this);
		screen.render();
		config = new Configuration();
		audio = new AudioEngine();
		audio.load();
	}


	public void setScreen(Screen screen, Screen old)
	{
		if(screen!=null){
			this.screen= screen;      
		}

		if(old!=null)
		{old.pause();
		old.dispose();
		old=null;}

		/*Non portable peut etre a cause de ce morceau de code*/

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Input getInput()
	{
		return input;
	}

	@Override
	public void dispose() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void render() {

		screen.render();
		screen.tick(input);
	}

	@Override
	public void resize(int width, int height) {
		screen.resize(width, height);
	}

	@Override
	public void resume() {

	}


	/**
	 * @return the audio
	 */
	public AudioEngine getAudio() {
		return audio;
	}
	
	

}