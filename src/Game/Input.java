package Game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class Input implements InputProcessor{
	public static final int UP = 0;
	public static final int DOWN = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;

	public static final int JUMP = 4;
	public static final int SHOOT = 5;

	public static final int ESCAPE = 6;

	public boolean[] buttons = new boolean[64];
	public boolean[] oldButtons = new boolean[64];

	int x=0;
	int y=0;
	boolean buttonDown=false;
	boolean isAccelerometerAvailable;
	boolean isAndroidApp;

	public Input()
	{
		super();
		releaseAllKeys();

	}

	public boolean allKeyReleased()
	{
		for(int i=0;i<64;i++)
		{
			if(buttons[i])
				return false;
		}
		return true;
	}
	public void set (int key, boolean down) {
		int button = -1;

		if (key == Keys.DPAD_UP) button = UP;
		if (key == Keys.DPAD_LEFT) button = LEFT;
		if (key == Keys.DPAD_DOWN) button = DOWN;
		if (key == Keys.DPAD_RIGHT) button = RIGHT;


		if (key == Keys.ESCAPE || key == Keys.MENU) button = ESCAPE;

		if (button >= 0 && button<64) {
			buttons[button] = down;
		}
	}

	public void releaseAllKeys () {
		for (int i = 0; i < buttons.length; i++) {
			if (i == UP || i == DOWN) continue;
			buttons[i] = false;
		}
	}

	@Override
	public boolean keyDown (int keycode) {
		set(keycode, true);
		return false;
	}

	@Override
	public boolean keyUp (int keycode) {
		set(keycode, false);
		return false;
	}

	@Override
	public boolean keyTyped (char character) {
		return buttons[character];
	}

	@Override
	public boolean touchDown (int x, int y, int pointer, int button) {
		this.x=x;
		this.y=y;
		
		buttonDown = true;
		return false;
	}

	@Override
	public boolean touchUp (int x, int y, int pointer, int button) {
		this.x=x;
		this.y=y;
		buttonDown=false;
		return false;
	}

	public boolean getButtonStatus()
	{
		return buttonDown;
	}

	@Override
	public boolean touchDragged (int x, int y, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved (int x, int y) {
		this.x=x;
		this.y=y;
		return false;
	}

	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}

	@Override
	public boolean scrolled (int amount) {
		return false;
	}



}
