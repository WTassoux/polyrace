package Game;

import android.util.Log;
import colorSet.ColorSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

import configuration.Configuration;

public class Tile {
	
	private Pixmap pixmap;
	private boolean isRoad;
	private TilePattern pattern;
	
	Tile(TilePattern pattern, Configuration conf){
		pixmap = new Pixmap(256, 256, Format.RGBA8888);
        this.isRoad = pattern.isRoad();
        this.pattern = pattern;
		Pixmap t;
		if(conf.colorSet == ColorSet.blackWhite){
			t = new Pixmap(Gdx.files.internal("tileSet3.jpg"));
		}
		else {
	        t = new Pixmap(Gdx.files.internal("tileSet2.jpg"));
		}
        pixmap.drawPixmap(t, pattern.getX()*128, pattern.getY()*128, 128, 128, 0, 0, 256, 256);
	}

	/**
	 * @return the pixmap
	 */
	public Pixmap getPixmap() {
		return pixmap;
	}
	
	public TilePattern getPattern() {
		return pattern;
	}

	/**
	 * @return the isRoad
	 */
	public boolean isRoad() {
		return isRoad;
	}
	
	
	
	

}
