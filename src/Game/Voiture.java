package Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Voiture {

    public static final float V_MAX = 220;
    private static final float ACCEL_PER_SEC = 18;
    private static final float DECCEL_PER_SEC = 30;
    private float vitesse;
    
    private Animation animation;
    private Animation animationBis;
    private final float tempsEtapeAnimation = 0.1f;
    private final int SPRITE_HEIGHT = 110;
    private final int SPRITE_WIDTH = 200;
    private final int SPRITE_NUMBER=3; //
    private TextureRegion tabRegion[];
    private TextureRegion tabRegionBis[];
    private TextureRegion currentRegion;
    private float currentTime;
    
    public Voiture(float v) {
        vitesse = v;
        currentTime=0f;
        Texture sprites = new Texture(Gdx.files.internal("tmp.png"));
        TextureRegion[][] tmp = TextureRegion.split(sprites, SPRITE_WIDTH, SPRITE_HEIGHT);
        tabRegion = new TextureRegion[SPRITE_NUMBER];
        tabRegionBis = new TextureRegion[SPRITE_NUMBER];
        int i=0;
        for(int j=0; j<3;j++)
        {
        	tabRegion[i++]=tmp[0][j];
        }
        i=0;
        for(int j=0;j<3;j++)
        {
        	tabRegionBis[i++]=tmp[1][j];
        }
        
        animationBis = new Animation(tempsEtapeAnimation, tabRegionBis);
        animation = new Animation(tempsEtapeAnimation, tabRegion);
    }
    
    public void render(SpriteBatch spriteBatch)
    {
    	currentTime += Gdx.graphics.getDeltaTime();
    	currentTime = Gdx.input.getAccelerometerY()/11f;
        
        if(currentTime>0)
        {
        	currentRegion = animation.getKeyFrame(currentTime, false);
        }
        else
        {
        	currentRegion = animationBis.getKeyFrame(-currentTime, false);
        }
        currentRegion.setRegion(currentRegion, 0, 0, SPRITE_WIDTH, SPRITE_HEIGHT);
        spriteBatch.draw(currentRegion, (Gdx.graphics.getWidth()-SPRITE_WIDTH)/2f, -20);
    
    }

    public Voiture() {
        this(0);
    }

    /**
     * @return the vitesse
     */
    public float getVitesse() {
        return vitesse;
    }

    /**
     * @param d
     *            the vitesse to set
     */
    public void setVitesse(double d) {
        this.vitesse = (float) d;
    }

    public void accelerer(float delta) {
        float newVitesse;

        newVitesse = vitesse + ACCEL_PER_SEC * delta;
        if (newVitesse <= V_MAX) {
            vitesse = newVitesse;
        } else {
            vitesse = V_MAX;
        }
    }

    public void decelerer(float delta) {
        float newVitesse;

        newVitesse = vitesse - DECCEL_PER_SEC * delta;
        if (newVitesse >= 0) {
            vitesse = newVitesse;
        } else {
            vitesse = 0;
        }
    }

    public void addMalus() {
        if (vitesse > V_MAX / 3) {
            vitesse -= 0.5;
        } else {
            vitesse = V_MAX / 3;
        }
    }

}
