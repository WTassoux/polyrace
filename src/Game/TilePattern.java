package Game;

import colorSet.ColorSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;

import configuration.Configuration;

public enum TilePattern {

	Tile00   (0,0,true),
	Tile01   (0,1,false),
	Tile02   (0,2,false),
	Tile03   (0,3,false),
	Tile04   (0,4,false),
	Tile10   (1,0,true),
	Tile11   (1,1,false),
	Tile12   (1,2,false),
	Tile13   (1,3,false),
	Tile14   (1,4,false),
	Tile20   (2,0,false),
	Tile21   (2,1,false),
	Tile22   (2,2,false),
	Tile23   (2,3,false),
	Tile24   (2,4,false),
	Tile30   (3,0,true),
	Tile31   (3,1,true),
	Tile32   (3,2,false),
	Tile33   (3,3,false),
	Tile34   (3,4,false),
	Tile40   (4,0,true),
	Tile41   (4,1,true),
	Tile42   (4,2,false),
	Tile43   (4,3,false),
	Tile44   (4,4,false);

    private boolean isRoad;
    int x;
    int y;
    
	TilePattern(int x, int y, boolean isRoad) {
	    this.isRoad = isRoad;
	    this.x=x;
	    this.y=y;
    }
	
    
	
	public boolean isRoad() {
	    return isRoad;
	}


	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}


	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	
}
