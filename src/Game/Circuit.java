package Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;

import configuration.Configuration;

public class Circuit {
	private int heigth;
	private int width;
	private Tile[][] tiles;
	private int departX;
	private int departY;
	private Texture texture;
	private ArrayList<Coordonnee> listeTiles;
	private int nbLap;
	private String name;
	private int actualTile;
		
	private Configuration config;
	
	public Circuit(String fileName, Configuration conf){
		config = conf;
		init(fileName);
		Gdx2DPixmap p = new Gdx2DPixmap(width, heigth, Gdx2DPixmap.GDX2D_FORMAT_RGBA8888);
		Pixmap circuit = new Pixmap(p);
		actualTile = 0;

		for(int i = 0; i<tiles.length;i++){
			for(int j = 0; j<tiles[i].length;j++){
				circuit.drawPixmap(tiles[i][j].getPixmap(), 0, 0, 256, 256, i*256, j*256, 256, 256);
			}
		}
		texture = new Texture(circuit);
		
		if(getDirectionToNextTile(departX,departY) == 1 || getDirectionToNextTile(departX,departY) ==3){
			Pixmap ligneArrive = new Pixmap(Gdx.files.internal("ligneArrivee.png"));
			texture.draw(ligneArrive, departX *256 +78, departY*256+128);
		}
		else if(getDirectionToNextTile(departX,departY) == 0 || getDirectionToNextTile(departX,departY) ==2){
			Pixmap ligneArrive = new Pixmap(Gdx.files.internal("ligneArriveeVerticale.png"));
			texture.draw(ligneArrive, departX *256+128, departY*256+78);
		}
		circuit.dispose();

	}
	private void init(String fileName){
		BufferedReader input;
		String line;
		String[] args;
		Coordonnee coordonneTile;
		FileHandle circuit = Gdx.files.internal(fileName);
		input = new BufferedReader(circuit.reader());
		try {
			while ((line = input.readLine()) != null) {
				args = line.split(";");
				if(args.length == 5){
					width = Integer.parseInt(args[0]) *256;
					heigth = Integer.parseInt(args[1]) * 256;
					tiles = new Tile[Integer.parseInt(args[0])][Integer.parseInt(args[1])];
					listeTiles = new ArrayList<Coordonnee>(Collections.nCopies(Integer.parseInt(args[2]), new Coordonnee(0,0)));
					nbLap = Integer.parseInt(args[3]);
					name = args[4];
				}
				else{
					tiles[Integer.parseInt(args[0])][Integer.parseInt(args[1])] = new Tile(TilePattern.valueOf(args[2]), config);

					if(Integer.parseInt(args[3]) == 0){
						departX = Integer.parseInt(args[0]);
						departY = Integer.parseInt(args[1]);
					}
					if(Integer.parseInt(args[3]) >= 0 ){
						coordonneTile = new Coordonnee(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
						listeTiles.set(Integer.parseInt(args[3]), coordonneTile);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getName()
	{
		return name;
	}
	/**
	 * @return the texture
	 */
	public Texture getTexture() {
		return texture;
	}
	/**
	 * @return the departX
	 */
	public int getDepartX() {
		return departX *256;
	}
	/**
	 * @return the departY
	 */
	public int getDepartY() {
		return departY *256;
	}
	private Coordonnee getNextTileCoordonnee(Coordonnee actualCoordonnee){
		if(listeTiles.indexOf(actualCoordonnee) == listeTiles.size()-1){
			return listeTiles.get(0);
		}
		else{
			return listeTiles.get(listeTiles.indexOf(actualCoordonnee)+1);
		}
	}

	public int getDirectionToNextTile(int x, int y){

		Coordonnee actualCoordonnee = new Coordonnee(x,y);


		if(actualCoordonnee.getX() > getNextTileCoordonnee(actualCoordonnee).getX() ){
			return 2;
		}
		else if(actualCoordonnee.getX() < getNextTileCoordonnee(actualCoordonnee).getX()){
			return 0;
		}
		else if(actualCoordonnee.getY() > getNextTileCoordonnee(actualCoordonnee).getY() ){
			return 3;
		}
		else if(actualCoordonnee.getY() < getNextTileCoordonnee(actualCoordonnee).getY() ){
			return 1;
		}
		else {
			return -1;
		}
	}
	
	
	/**
	 * @param x, y
	 * @return 1 si le virage est � gauche
	 * 		   -1 si le virage est � droite
	 * 		   0 sinon
	 */
	
	public int getNextTurn(int x, int y){
		Coordonnee actualCoordonnee = new Coordonnee(x,y);
		
		Coordonnee nextCoordonnee = new Coordonnee(getNextTileCoordonnee(actualCoordonnee).getX(), getNextTileCoordonnee(actualCoordonnee).getY());
		
		if(actualCoordonnee.getX() > nextCoordonnee.getX() ){ //d�placement vers la gauche
			if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile30 ){
				return 1;//gauche
			}
			else if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile31){
				return -1;//droite
			}
			else return 0;
		}
		
		else if(actualCoordonnee.getX() < nextCoordonnee.getX()){//d�placement vers la droite
			if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile40 ){
				return -1;
			}
			else if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile41){
				return 1;
			}
			else return 0;
		}
		
		else if(actualCoordonnee.getY() > nextCoordonnee.getY() ){//d�placement vers le haut
			if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile30 ){
				return -1;
			}
			else if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile40){
				return 1;
			}
			else return 0;
		}
		
		else if(actualCoordonnee.getY() < nextCoordonnee.getY() ){//d�placement vers le bas
			if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile31 ){
				return 1;
			}
			else if(tiles[nextCoordonnee.getX()][nextCoordonnee.getY()].getPattern() == TilePattern.Tile41){
				return -1;
			}
			else return 0;
		}
		
		else {
			return 42;
		}
		
	}

	public int getAngleDepart(){
		
		int direction = getDirectionToNextTile(listeTiles.get(0).getX(), listeTiles.get(0).getY());
		
		if(direction == 3 ){
			return 270;
		}
		else if(direction == 1 ){
			return 90;
		}
		else if(direction == 0){
			return 0;
		}
		else if(direction == 2){
			return 180;
		}
		else {
			return -1;
		}
	}
	
	public Tile tileAt(float x, float y) {
	    return tiles[(int)(x / 256)][(int)(y / 256)];
	}
	
	public boolean isNewTile(int x, int y){
		Coordonnee coord = new Coordonnee(x, y);
		if(listeTiles.indexOf(coord) == actualTile){
			return false;
		}
		else{
			actualTile = listeTiles.indexOf(coord);
			return true;
		}
	}
	public boolean isLapOver(int compt) {
		if(compt == listeTiles.size()){
			return true;
		}
		else{
			return false;
		}
	}
	public int getnbLap() {
		return nbLap;
	}
	public boolean end(TilePattern t)
	{
		if(t.equals(listeTiles.get(listeTiles.size()-1)))
		{
			return true;
		}
		return false;
	}
	
	public void dispose()
	{
		texture.dispose();
	}
}
