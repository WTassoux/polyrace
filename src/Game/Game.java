package Game;

import java.io.BufferedReader;
import java.util.Stack;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class Game implements ApplicationListener /*ApplicationAdapter*/{
	
	OrthographicCamera camera;
    ShaderProgram shaders;
    Texture texture;
    Texture circuit;
    SpriteBatch batch;
    Mesh fullscreenQuad;
    float angle;

    public void createFullScreenQuad() {

        float[] verts = new float[12];
        int i = 0;

        verts[i++] = -1f;
        verts[i++] = -1f;
        verts[i++] = 0;

        verts[i++] = 1f;
        verts[i++] = -1f;
        verts[i++] = 0;

        verts[i++] = 1f;
        verts[i++] = 1f;
        verts[i++] = 0;

        verts[i++] = -1f;
        verts[i++] = 1f;
        verts[i++] = 0;

        fullscreenQuad = new Mesh(true, 4, 0, new VertexAttribute( VertexAttributes.Usage.Position , 3, "vPosition"));

        fullscreenQuad.setVertices(verts);
    }

    public void create() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
        FileHandle VSShaderFile;
        FileHandle PSShaderFile;
        String VSShader;
        String PSShader;
        BufferedReader input;

        VSShader = new String();
        PSShader = new String();
        VSShaderFile = Gdx.files.internal("Mode7RendererVS.glsl");
        PSShaderFile = Gdx.files.internal("Mode7RendererPS.glsl");
        if (!PSShaderFile.exists() || !VSShaderFile.exists())
            throw new GdxRuntimeException("Cannot read shaders");
        if (!Gdx.app.getGraphics().isGL20Available())
            throw new GdxRuntimeException("OpenGLES 2 needed");

        try {
            String line;

            input = new BufferedReader(VSShaderFile.reader());
            while ((line = input.readLine()) != null) {
                VSShader = VSShader + line
                        + System.getProperty("line.separator");
            }
            input.close();
            input = new BufferedReader(PSShaderFile.reader());
            while ((line = input.readLine()) != null) {
                PSShader = PSShader + line
                        + System.getProperty("line.separator");
            }
            input.close();

        } catch (Exception e) {
            throw new GdxRuntimeException("Can't read shaders");
        }

        angle = 0.f;
        createFullScreenQuad();
        circuit = new Texture(Gdx.files.internal("circuit.png"));

        shaders = new ShaderProgram(VSShader, PSShader);
        if (!shaders.isCompiled())
            throw new GdxRuntimeException("Error compiling shaders "
                    + shaders.getLog());
        circuit.bind(0);
    }

    public void render() {
        shaders.begin();
        shaders.setUniformi("trackTexture", 0);
        shaders.setUniformf("screenSize", new Vector2(Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight()));
        shaders.setUniformf("trackTextureSize", new Vector2(circuit.getWidth(),
                circuit.getHeight()));
        shaders.setUniformf("windowCenter", new Vector2(
                Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2));
        shaders.setUniformf("cameraFov", (float) Math.toRadians(60));
        shaders.setUniformf("cameraScreenPosition", new Vector2(255, 128));
        shaders.setUniformf("deltaAngle",
                (float) (Math.toRadians(60) / Gdx.graphics.getWidth()));
        shaders.setUniformf("cameraPosition", new Vector2(
                circuit.getWidth() / 2, circuit.getHeight() / 2));
        shaders.setUniformf("cameraAngle", (float) Math.toRadians(angle));
        fullscreenQuad.render(shaders, GL20.GL_TRIANGLE_FAN);
        shaders.end();
        ++angle;
        Gdx.app.log("Framerate", "FPS: " + Gdx.graphics.getFramesPerSecond());
    }

    public void resize(int width, int height) {
    }

    public void pause() {
    }

    public void resume() {
    }

    public void dispose() {
    }
	
}
