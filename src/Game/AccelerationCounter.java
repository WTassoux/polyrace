package Game;

import android.graphics.Color;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AccelerationCounter {

	Pixmap bar;
	Gdx2DPixmap p;
	Texture current;
	Pixmap currentBar;
	
	public AccelerationCounter()
	{
		bar = new Pixmap(Gdx.files.internal("accelerationCounter.png"));
		p = new Gdx2DPixmap(20, Gdx.graphics.getHeight(), Gdx2DPixmap.GDX2D_FORMAT_RGBA8888);
		currentBar = new Pixmap(p);
		currentBar.setColor(Color.BLACK);
		currentBar.fill();
		current = new Texture(currentBar);
	}
	
	public Texture getCurrentSpeed(int speed)
	{
		currentBar.fill();
	 	currentBar.drawPixmap(bar, 0, 0, 0, 0, 10, (int)(bar.getHeight()*(speed/(float)Voiture.V_MAX)));
	 	current.draw(currentBar, 0, 0);
		return current;
				
	}
}
