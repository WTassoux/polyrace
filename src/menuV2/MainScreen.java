package menuV2;

import screen.TrackSelection;
import Game.Application;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class MainScreen extends MenuV2 {

	public MainScreen(Application app) {
		super(app);
		TextButton jouer = new TextButton("Jouer", getSkin());
		jouer.setTouchable(Touchable.enabled);
		jouer.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		jouer.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				if(application.config.audio){
					application.getAudio().playJouer();
				}
				application.setScreen(new TrackSelection(application, current),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(jouer);

		/*
		TextButton inGame = new TextButton("lancer le circuit", getSkin());
		inGame.setTouchable(Touchable.enabled);
		inGame.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		inGame.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				application.setScreen(new InGame(),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(inGame);
		 */

		TextButton option = new TextButton("Options", getSkin());
		option.setTouchable(Touchable.enabled);
		option.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		option.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				if(application.config.audio){
					application.getAudio().playOptions();
				}
				application.setScreen(new OptionScreen(application),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(option);


		TextButton quit = new TextButton("Quitter", getSkin());
		quit.setTouchable(Touchable.enabled);
		quit.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		quit.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				if(application.config.audio){
					application.getAudio().playQuitter();
				}
				Gdx.app.exit();
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(quit);

		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		addButtonToStage();
	}

}
