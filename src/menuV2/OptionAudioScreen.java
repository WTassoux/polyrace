package menuV2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import Game.Application;

public class OptionAudioScreen extends MenuV2 {

	//faire fichier config;
	boolean isActived = true;
	TextButton audioHelp;

	public OptionAudioScreen(Application app) {
		super(app);
		// TODO Auto-generated constructor stub

		audioHelp = new TextButton("Aide activee", getSkin());
		audioHelp.setTouchable(Touchable.enabled);
		audioHelp.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		audioHelp.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				isActived = !isActived;
				if(isActived)
				{
					audioHelp.setText("Aide activee");
					if(application.config.audio){
						application.getAudio().playAideActivee();
					}
					application.config.audio=true;
				}
				else
				{
					if(application.config.audio){
						application.getAudio().playAideDesactivee();
					}
					audioHelp.setText("Aide desactivee");
					application.config.audio=false;
				}
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(audioHelp);

		TextButton back = new TextButton("Revenir", getSkin());
		back.setTouchable(Touchable.enabled);
		back.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		back.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				application.setScreen(new OptionScreen(application),current);
				if(application.config.audio){
					application.getAudio().playRevenir();
				}
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(back);

		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());


		addButtonToStage();
	}

}
