package menuV2;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import Game.Application;

public class OptionScreen extends MenuV2 {

	public OptionScreen(Application app) {
		super(app);
		configureButtonPosition();

		TextButton color = new TextButton("Couleurs", getSkin());
		color.setTouchable(Touchable.enabled);
		color.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		color.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {

				if(application.config.audio){
					application.getAudio().playCouleur();
				}
				application.setScreen(new OptionColorScreen(application),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(color);


		TextButton audioHelp = new TextButton("Aide audio", getSkin());
		audioHelp.setTouchable(Touchable.enabled);
		audioHelp.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		audioHelp.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				if(application.config.audio){
					application.getAudio().playAideAudio();
				}
				application.setScreen(new OptionAudioScreen(application),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(audioHelp);

		TextButton back = new TextButton("Revenir", getSkin());
		back.setTouchable(Touchable.enabled);
		back.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		back.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				if(application.config.audio){
					application.getAudio().playRevenir();
				}
				application.setScreen(new MainScreen(application),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(back);

		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		addButtonToStage();
	}

}
