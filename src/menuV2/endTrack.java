package menuV2;

import screen.Screen;
import screen.TrackSelection;
import Game.Application;
import chronometer.Chronometer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class endTrack extends MenuV2 {
	private BitmapFont font;
	Chronometer chrono;
	Screen sc = this;
	public endTrack(Application app, Chronometer chrono,String circuitName) {
		super(app);
		this.chrono=chrono;
		font = new BitmapFont(Gdx.files.internal("skins/arial-bold-75pts.fnt"),Gdx.files.internal("skins/arial-bold-75pts.png"),false);
		font.setColor(Color.RED);


		TextButton mainMenu= new TextButton("Menu principal", getSkin());
		mainMenu.setTouchable(Touchable.enabled);
		mainMenu.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		mainMenu.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				application.setScreen(new MainScreen(application),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(mainMenu);

		TextButton trackSelection = new TextButton("Selection circuit", getSkin());
		trackSelection.setTouchable(Touchable.enabled);
		trackSelection.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		trackSelection.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				application.setScreen(new TrackSelection(application,sc),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(trackSelection);

		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		addButtonToStage();
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		spriteBatch.begin();
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		//chrono.displaytime(spriteBatch, Gdx.graphics.getWidth()/2 , (int)(Gdx.graphics.getHeight()-BUTTON_HEIGHT));
		spriteBatch.end();
		
		stage.draw();
		
		spriteBatch.begin();
		chrono.drawTime(spriteBatch, Gdx.graphics.getWidth()*1/4, (int)(Gdx.graphics.getHeight()-4));
		spriteBatch.end();
	}
}
