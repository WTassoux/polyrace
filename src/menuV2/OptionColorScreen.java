package menuV2;

import colorSet.ColorSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import Game.Application;

public class OptionColorScreen extends MenuV2 {

	ColorSet colorSet = ColorSet.normal;
	TextButton set1;
	TextButton set2;
	public OptionColorScreen(Application app) {
		super(app);
		// TODO Auto-generated constructor stub

		set1 = new TextButton("Couleur active", getSkin());
		set1.setTouchable(Touchable.enabled);
		set1.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		set1.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				if(application.config.audio){
					application.getAudio().playNormale();
				}
				set1.setText("Couleur active");
				set2.setText("Noir/blanc desact");
				application.config.colorSet=ColorSet.normal;

				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(set1);

		set2 = new TextButton("Noir/blanc", getSkin());
		set2.setTouchable(Touchable.enabled);
		set2.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		set2.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				if(application.config.audio){
					application.getAudio().playNoirEtBlanc();
				}
				set1.setText("Couleur" +
						" desact");
				set2.setText("Noir/blanc active");
				application.config.colorSet=ColorSet.blackWhite;
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(set2);

		TextButton back = new TextButton("Revenir", getSkin());
		back.setTouchable(Touchable.enabled);
		back.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		back.addListener(new InputListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				//application.dispose();
				if(application.config.audio){
					application.getAudio().playRevenir();
				}
				application.setScreen(new OptionScreen(application),current);
				return true;  // must return true for touchUp event to occur
			}
		}
				);
		buttons.add(back);

		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		addButtonToStage();
	}



}
