package menuV2;

import java.util.ArrayList;

import screen.Screen;
import Game.Application;
import Game.Input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class MenuV2 extends Screen {

	protected ArrayList<TextButton> buttons;
	protected Application application;
	protected static float BUTTON_WIDTH = 300f;
	protected static float BUTTON_HEIGHT = 60f;
	protected static float BUTTON_SPACING = 10f;
	private Skin skin;
	private float xbutton;
	private float ybutton;
	protected Screen current = this;
	
	public MenuV2(Application app)
	{
		super(app);
		Gdx.input.setInputProcessor(stage);
		buttons = new ArrayList<TextButton>();
		application = app;
		FileHandle f1 = Gdx.files.internal("skins/uiskin.json");
		skin = new Skin(f1);
		
		//resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		configureButtonPosition();
		
		
	}
	protected Skin getSkin()
	{
		return skin;
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width,height);
		
		configureButtonPosition();
		resizeButton(width, height);	
		stage.draw();
	}
	
	protected void configureButtonPosition()
	{
		xbutton=(Gdx.graphics.getWidth()-BUTTON_WIDTH)/2f;
		int i=0;
		for(TextButton b : buttons)
		{
			b.setPosition(xbutton, Gdx.graphics.getHeight()-(i*BUTTON_HEIGHT+BUTTON_SPACING));
			i++;
		}
	}
	
	protected void resizeButton(int width, int height)
	{
		BUTTON_HEIGHT = height/(buttons.size()+2);
		BUTTON_WIDTH = width-(float)(2*width/8f);
		int i=1;
		for(TextButton b: buttons)
		{
			b.setY(height-height/5-i*BUTTON_HEIGHT-i*BUTTON_SPACING);
			b.setX(width/8);
			b.setWidth(BUTTON_WIDTH);
			b.setHeight(BUTTON_HEIGHT);
			i++;
		}
	}
	
	protected void addButtonToStage()
	{
		for(TextButton b: buttons)
		{
			b.setColor(Color.YELLOW);
			b.getLabel().setFontScale(1.5f);
			stage.addActor(b);
		}
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		for(TextButton b: buttons)
		{
			b.setTouchable(Touchable.disabled);
		}
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
		stage.draw();
	}
	
	@Override
	public void render() {
		// TODO Auto-generated method stub
		spriteBatch.begin();
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		spriteBatch.end();
		
		stage.draw();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}


	@Override
	public void tick(Input input) {
		// TODO Auto-generated method stub

	}

}
