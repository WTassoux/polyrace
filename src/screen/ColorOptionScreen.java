package screen;

import com.badlogic.gdx.Gdx;

import Game.Application;
import menu.ButtonMenu;
import menu.Menu;

public class ColorOptionScreen extends Menu {

	public ColorOptionScreen(Application app) {
		// TODO Auto-generated constructor stub
		super(app);
		buttons.add(new ButtonMenu("Quitter", getSkin()));
		
		addButtonToStage();
		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
	}
}
