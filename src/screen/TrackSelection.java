package screen;

import java.util.ArrayList;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

import Game.Application;
import Game.Circuit;
import Game.Input;

public class TrackSelection extends Screen {

	private static float BUTTON_WIDTH = 0;
	private static float BUTTON_HEIGHT = 0;
	private float scaleHeight = 4f / 6f;
	private float scaleWidth = 1f / 4f;

	TextButton previous;
	TextButton next;
	TextButton validateTrack;
	private Skin skin;
	private TextButtonStyle buttonStyle;

	private Label title;
	private LabelStyle labelStyle;

	private ArrayList<Texture> circuits;
	private int currentCircuit;
	private Texture track;
	private Image image;

	private int initial_width;
	private int initial_height;

	public TrackSelection(final Application app, final Screen current) {
		super(app);
		Gdx.input.setInputProcessor(stage);
		initial_width = Gdx.graphics.getWidth();
		initial_height = Gdx.graphics.getHeight();
		currentCircuit = 0;

		initializeCircuitTable();
		track = circuits.get(0);
		// Texture track = new Texture(Gdx.files.internal("circuit.png"));
		image = new Image(circuits.get(currentCircuit));

		FileHandle skinFileHandle = Gdx.files.internal("skins/uiskin.json");
		skin = new Skin(skinFileHandle);

		buttonStyle = setButtonStyle();

		previous = new TextButton("Prece\ndent", skin);
		next = new TextButton("Suivant", skin);

		/* Valider */

		validateTrack = new TextButton("lancer le circuit", skin);
		validateTrack.setColor(Color.RED);

		validateTrack.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (application.config.audio) {
					application.getAudio().playLancerCircuit();
				}

				for (Texture c : circuits) {
					c.dispose();
				}
				app.setScreen(new InGame("testCircuit" + (currentCircuit + 1)
						+ ".txt", app), current);

				return true; // must return true for touchUp event to occur
			}
		});

		next.setTouchable(Touchable.enabled);
		next.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (application.config.audio) {
					application.getAudio().playSuivant();
				}
				currentCircuit++;
				if (currentCircuit == -1) {
					currentCircuit = circuits.size() - 1;
				} else if (currentCircuit == circuits.size()) {
					currentCircuit = 0;
				} else {
					currentCircuit %= circuits.size();
				}

				track = circuits.get(currentCircuit);
				image = new Image(track);

				return true; // must return true for touchUp event to occur
			}
		});
		next.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);

		previous.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				if (application.config.audio) {
					application.getAudio().playPrecedent();
				}
				currentCircuit--;
				if (currentCircuit == -1) {
					currentCircuit = circuits.size() - 1;
				} else if (currentCircuit == circuits.size()) {
					currentCircuit = 0;
				} else {
					currentCircuit %= circuits.size();
				}

				track = circuits.get(currentCircuit);
				image = new Image(track);

				return true; // must return true for touchUp event to occur
			}
		});
		previous.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);

		labelStyle = new LabelStyle();
		title = new Label("seletion du circuit", skin);

	}

	private void initializeCircuitTable() {
		int i = 1;
		Texture temp;

		String cheminCircuit = "miniature" + i + ".png";
		FileHandle circuitHandler = Gdx.files.internal(cheminCircuit);

		circuits = new ArrayList<Texture>();

		// On ajoute tout les circuits! Les circuits ont la forme
		// testCircuitI.txt avec I numéro du circuit
		do {
			temp = new Texture(circuitHandler);
			circuits.add(temp);
			i++;

			cheminCircuit = "miniature" + i + ".png";
			circuitHandler = Gdx.files.internal(cheminCircuit);
		} while (circuitHandler.exists());
	}

	// nullpointerexception...
	public TextButtonStyle setButtonStyle() {
		TextButtonStyle style = new TextButtonStyle();
		// FileHandle fontPNG = Gdx.files.internal("skins/default.png");
		// FileHandle fontFNT = Gdx.files.internal("skins/default.fnt");
		// buttonStyle.font = new BitmapFont(fontPNG,fontFNT);
		style.font = new BitmapFont(false);
		style.fontColor = Color.WHITE;
		style.pressedOffsetY = 1f;
		style.downFontColor = new Color(0.8f, 0.8f, 0.8f, 1f);
		return style;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		spriteBatch.begin();
		spriteBatch.end();
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);

		BUTTON_WIDTH = width * scaleWidth;
		BUTTON_HEIGHT = height * scaleHeight;
		next.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		previous.setBounds(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
		displayCurrentTrack(width, height);
		displayButton(width, height);
	}

	private void displayCurrentTrack(int width, int height) {
		image.setPosition(BUTTON_WIDTH, 0);
		image.setHeight(BUTTON_HEIGHT);
		image.setWidth((width - 2 * BUTTON_WIDTH));

		spriteBatch.begin();
		// image.draw(spriteBatch,1f);
		spriteBatch.draw(track, initial_width / 4f, 0, initial_width - 2
				* initial_width / 4f, initial_height * 4f / 6f);
		spriteBatch.end();
	}

	private void displayButton(int width, int height) {
		previous.setHeight(BUTTON_HEIGHT);
		previous.setWidth(BUTTON_WIDTH);
		previous.setX(0);
		previous.setY(0);

		next.setHeight(BUTTON_HEIGHT);
		next.setWidth(BUTTON_WIDTH);
		next.setX(width - BUTTON_WIDTH);
		next.setY(0);
		next.setColor(Color.GREEN);
		next.getLabel().setAlignment(Align.center);

		validateTrack.setPosition(0, height - BUTTON_HEIGHT / 2);
		validateTrack.setHeight(height - BUTTON_HEIGHT);
		validateTrack.setWidth(width);

		stage.addActor(validateTrack);
		stage.addActor(previous);
		stage.addActor(next);

		stage.draw();
	}

	@Override
	public void tick(Input input) {
		// TODO Auto-generated method stub

	}

}
