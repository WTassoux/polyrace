package screen;

import Game.Input;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
public class Option extends Screen{

	int x=0;
	int y=0;
	Screen parent;
	Texture curseur;

	public Option() {
		curseur = new Texture(Gdx.files.internal("Curseur.png"));
	}

	public Option(Screen parent) {
		this.parent = parent;
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		spriteBatch.begin();
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		spriteBatch.draw(curseur,x,y);
		spriteBatch.end();  
	}

	@Override
	public void tick(Game.Input input) {
		/*if (!input.oldButtons[Input.ESCAPE] && input.buttons[Input.ESCAPE]) {
			setScreen(parent);
			return;
		}*/
		if(Gdx.app.getType()!=ApplicationType.Android){
			if(!input.allKeyReleased()){
				if (input.keyTyped((char) Input.UP) && input.oldButtons[Input.UP] ) {
					y--;
				}
				if (input.keyTyped((char) Input.DOWN) && input.buttons[Input.DOWN]) {
					y++;
				}
				if (input.keyTyped((char) Input.LEFT) && input.buttons[Input.LEFT]) {
					x--;
				}
				if (input.keyTyped((char) Input.RIGHT) && input.buttons[Input.RIGHT]) {
					x++;
				}
			}
		}
		else{

			if(Gdx.input.getAccelerometerZ() > 1){
				y--;

			}
			if(Gdx.input.getAccelerometerZ() < -1){
				y++;
			}
			if(Gdx.input.getAccelerometerY() > 1){
				x++;
			}
			if(Gdx.input.getAccelerometerY() < -1){
				x--;
			}

		}
		/*if (input.buttons[Input.SHOOT] && !input.oldButtons[Input.SHOOT]) {
			if (selected == 0) {
				setScreen(parent);
			} else if (selected == 1) {
				parent.level.player.die();
				setScreen(parent);
			} else if (selected == 2) {
				setScreen(new TitleScreen());
			} else if (selected == 3) {
				setScreen(new WinScreen());
			}
		}*/
		// if (delay>0) delay--;
		// if (delay==0 && input.buttons[Input.SHOOT] && !input.oldButtons[Input.SHOOT]) {
		// setScreen(parent);
		// }
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}
}


