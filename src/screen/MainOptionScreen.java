package screen;

import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import menu.ButtonMenu;
import menu.Menu;
import Game.Application;

public class MainOptionScreen extends Menu {

	public MainOptionScreen(Application app){
		super(app);
		buttons.add(new ButtonMenu("Changer couleur",getSkin()));
		buttons.add(new ButtonMenu("Aide audio",getSkin()));
		buttons.add(new ButtonMenu("Revenir",getSkin()));

		//A corriger ................................................+gerer les disposes
		screens.add(new ColorOptionScreen(application));
		screens.add(new AudioOptionScreen(application));
		screens.add(new AudioOptionScreen(application));
		
		addButtonToStage();
		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
	}

}
