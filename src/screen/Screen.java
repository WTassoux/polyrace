package screen;

import Game.Application;
import Game.Input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public abstract class Screen implements com.badlogic.gdx.Screen {
	
	private final String[] chars = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", ".,!?:;\"'+-=/\\< "};
	protected Application application;
	protected SpriteBatch spriteBatch;
	
    protected final Stage stage;

	public void removed() {
		spriteBatch.dispose();
	}
	
	public Screen()
	{
		spriteBatch = new SpriteBatch();
		stage = new Stage(0,0,true);
	
	}
	
	public Screen(Application app)
	{
		spriteBatch = new SpriteBatch();
		application = app;
		stage = new Stage(0,0,true);
	}

	public final void init(Application app) {
		this.application = app;
	}

	public void draw (TextureRegion region, int x, int y) {
		int width = region.getRegionWidth();
		if (width < 0) width = -width;
		spriteBatch.draw(region, x, y, width, region.getRegionHeight());
	}

	public void drawString(String string, int x, int y) {
		string = string.toUpperCase();
		for (int i = 0; i < string.length(); i++) {
			char ch = string.charAt(i);
			for (int ys = 0; ys < chars.length; ys++) {
				int xs = chars[ys].indexOf(ch);
				if (xs >= 0) {
					//draw(Art.guys[xs][ys + 9], x + i * 6, y);
				}
			}
		}
		Gdx.graphics.setDisplayMode(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(), true);
	}

	public abstract void render();

	public abstract void tick(Input input);
	
	public void resize(int width, int height)
	{
		stage.setViewport( width, height, true );
        //stage.clear();
	}
}
