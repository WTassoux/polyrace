package screen;

import menu.ButtonMenu;
import menu.Menu;
import Game.Application;

import com.badlogic.gdx.Gdx;

public class MenuScreen extends Menu{
		
	public MenuScreen(Application app)
	{
		super(app);
		//InGame inGame = new InGame();
		
		buttons.add(new ButtonMenu("Jouer", getSkin()));
		buttons.add(new ButtonMenu("Options",getSkin()));
		buttons.add(new ButtonMenu("Quitter",getSkin()));
		
		//screens.add(inGame);
		screens.add(new MainOptionScreen(app));	
		screens.add(new MainOptionScreen(app));
		
		addButtonToStage();
		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		
	}
	
	

}
