package screen;

import com.badlogic.gdx.Gdx;

import menu.ButtonMenu;
import menu.Menu;
import Game.Application;
import Game.Input;

public class AudioOptionScreen extends Menu {
	
	public AudioOptionScreen(Application app)
	{
		super(app);
		application = app;
		buttons.add(new ButtonMenu("Jouer", getSkin() ));
		optionNumber++;
		
		addButtonToStage();
		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
	}
	
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tick(Input input) {
		// TODO Auto-generated method stub
		
	}

}
