package AudioEngine;

import org.lwjgl.opengl.XRandR;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AudioEngine {
	
	Music backgroundMusic;
	Music moteur;
	Sound maxAcceleration;
	Music acceleration;
	Music freinage;
	Music wrongWay;
	Music tournezAGauche;
	Music tournezADroite;
	Music tuto;
	Music jouer;
	Music aideActivee;
	Music aideAudio;
	Music noirEtBlanc;
	Music normale;
	Music options;
	Music quitter;
	Music revenir;
	Music couleur;
	Music aideDesactivee;
	Music precedent;
	Music suivant;
	Music lancerCircuit;
	
	
	boolean dec=false;
	public void load()
	{
		moteur = getNewMusic("moteur.wav");
		acceleration = getNewMusic("Jacceleration.mp3");
		//maxAcceleration = getNewSound("");
		freinage = getNewMusic("-1_F430_sim.wav");
		wrongWay = getNewMusic("WrongWay.mp3");
		tournezAGauche = getNewMusic("tournezAGauche.mp3");
		tournezADroite = getNewMusic("tournezADroite.mp3");
		tuto = getNewMusic("tuto.mp3");
		tuto.setLooping(false);
		jouer = getNewMusic("jouer.mp3");
		aideActivee = getNewMusic("aideActivee.mp3");
		aideAudio = getNewMusic("aideAudio.mp3");
		noirEtBlanc = getNewMusic("noirEtBlanc.mp3");
		normale = getNewMusic("normale.mp3");
		options = getNewMusic("options.mp3");
		revenir = getNewMusic("revenir.mp3");
		quitter = getNewMusic("quitter.mp3");
		couleur = getNewMusic("couleur.mp3");
		aideDesactivee = getNewMusic("aideDesactivee.mp3");
		precedent = getNewMusic("precedent.mp3");
		suivant = getNewMusic("suivant.mp3");
		lancerCircuit = getNewMusic("lancerCircuit.mp3");
	}
	
	private Music getNewMusic(String s)
	{
		return Gdx.audio.newMusic(Gdx.files.internal("sounds/"+s));
	}
	
	private Sound getNewSound(String s)
	{
		return Gdx.audio.newSound(Gdx.files.internal("sounds/"+s));
	}
	
	public void pause()
	{
		acceleration.stop();
		freinage.stop();
		moteur.stop();
		wrongWay.stop();
	}
	
	public void playMoteur()
	{
		moteur.setVolume(0.99f);
		moteur.play();
		moteur.setLooping(true);
	}
	
	public void acceleration()
	{
		freinage.stop();
		acceleration.play();
		acceleration.setVolume(0.9f);
		dec=false;
		
	}
	
	public void playBackground()
	{
		//backgroundMusic.play();
		//backgroundMusic.setVolume(0.4f);
		//backgroundMusic.setLooping(true);
	}

	public void deceleration(float vitesse) {
		acceleration.stop();
		if(vitesse>30 && !dec )
		{
			freinage.setVolume(0.7f);
			freinage.play();
			dec=true;
		}
	}
	
	public void playWrongWay(){
		wrongWay.play();
		wrongWay.setVolume(0.4f);
		wrongWay.setLooping(true);
	}
	public void stopWrongWay(){
		wrongWay.stop();
	}
	
	public void playTournezADroite(){
		tournezADroite.play();
		tournezADroite.setVolume(0.7f);
	}
	
	public void playTournezAGauche(){
		tournezAGauche.play();
		tournezAGauche.setVolume(0.7f);
	}
	
	public void playJouer(){
		jouer.play();
	}

	
	public void playAideActivee(){
		aideActivee.play();
	}

	
	public void playAideAudio(){
		aideAudio.play();
	}
	
	public void playNoirEtBlanc(){
		noirEtBlanc.play();
	}
	
	public void playNormale(){
		normale.play();
	}
	
	public void playOptions(){
		options.play();
	}
	
	public void playRevenir(){
		revenir.play();
	}

	
	public void playQuitter(){
		quitter.play();
	}

	public void playTuto() {
		tuto.play();
	}

	public void stopTuto() {
		tuto.stop();
		
	}
	
	public void playCouleur() {
		couleur.play();
	}
	public void playAideDesactivee() {
		aideDesactivee.play();
	}
	public void playPrecedent() {
		precedent.play();
	}
	public void playSuivant() {
		suivant.play();
	}
	public void playLancerCircuit() {
		lancerCircuit.play();
	}

}
