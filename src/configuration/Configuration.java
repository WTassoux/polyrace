package configuration;

import colorSet.ColorSet;

public class Configuration {
	public boolean audio;
	public ColorSet colorSet;
	public boolean displayCar;
	public boolean displaySteeringWheel;

	public Configuration()
	{
		colorSet=ColorSet.normal;
		audio = true;
		displayCar=true;
		displaySteeringWheel=false;
	}

	public void load()
	{
		//...parser fichier txt ou jdom ou autre ...
	}
}
