package chronometer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;

public class Chronometer {
	int hours;
	int minutes;
	long milliSeconds;
	int seconds;
	static long beginningTime;
	private BitmapFont font;
	private boolean running;

	public Chronometer() {
		font = new BitmapFont(Gdx.files.internal("skins/arial-bold-75pts.fnt"),
				Gdx.files.internal("skins/arial-bold-75pts.png"), false);
		font.setColor(Color.RED);

		beginningTime = TimeUtils.millis();
	}

	public void reset() {
		beginningTime = TimeUtils.millis();
	}

	public void updateAndDisplay(SpriteBatch batch) {

		if (running) {
			long elapsedTime = (TimeUtils.millis() - beginningTime) / 1000;
			
			seconds = (int)elapsedTime % 60;
			minutes = (((int)elapsedTime) / 60) % 60;
			hours = (((int)elapsedTime) / 3600); 
		}
		
		// on dessine "en dehors de l'ecran" a cause de la font, l'intergne est
		// trop grand donc on ajuste
		drawTime(batch, (int)(Gdx.graphics.getWidth() * 1.f/4.f), Gdx.graphics.getHeight());
	}

	public String getTime() {
		return hours + "h" + minutes + "m" + (int) seconds + 's';
	}

	public void stop() {
		running = false;
	}

	public void drawTime(SpriteBatch sp, int x, int y) {
		font.draw(sp, fixedWidth(hours) + "h" + fixedWidth(minutes) + "m" + fixedWidth(seconds) + 's', x, y);
	}
	
	private String fixedWidth(int integer)
	{
		if (integer < 10)
			return ("0" + integer);
		else
			return Integer.toString(integer);
	}

	public void begin() {
		running = true;
	}
}
