package menu;

import java.util.ArrayList;

import screen.Screen;
import Game.Application;
import Game.Input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public abstract class Menu extends Screen{

	protected ArrayList<ButtonMenu> buttons;
	protected ArrayList<Screen> screens;
	protected Application application;
	protected int optionNumber=0;
	protected static float BUTTON_WIDTH = 300f;
	protected static float BUTTON_HEIGHT = 60f;
	protected static float BUTTON_SPACING = 10f;
	private Skin skin;
	
	
	public Menu(Application app)
	{
		FileHandle f1 = Gdx.files.internal("skins/uiskin.json");
		skin = new Skin(f1);
		application = app;
		buttons=new ArrayList<ButtonMenu>();
		screens = new ArrayList<Screen>();
		resize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
	}
	
	public Skin getSkin()
	{
		return skin;
	}
	
	protected void addButtonToStage()
	{
		for(ButtonMenu b: buttons)
		{
			stage.addActor(b);
			b.getLabel().setFontScale(4.0f);
			b.align(0);
		}
		optionNumber=buttons.size();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width,height);
		resizeButton(width, height);	
		stage.draw();
	}

	private void resizeButton(int width, int height)
	{
		BUTTON_HEIGHT = height/(optionNumber+3);
		BUTTON_WIDTH = width-(float)(2*width/8f);
		int i=1;
		for(ButtonMenu b: buttons)
		{
			b.setY(height-height/5-i*BUTTON_HEIGHT-i*BUTTON_SPACING);
			b.setX(width/8);
			b.setWidth(BUTTON_WIDTH);
			b.setHeight(BUTTON_HEIGHT);
			i++;
		}
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		spriteBatch.begin();
		spriteBatch.end();
		Gdx.gl.glClearColor( 0f, 0f, 0f, 1f );
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT );
		stage.draw();
	}

	//@Override
	public void tick(Input input) {

		int i=0;
		for(ButtonMenu b: buttons)
		{
			if(input.getButtonStatus() && buttons.size()>0 && screens.size()>0)
			{
				if(b.isMouseOverButton(input.getX(), Gdx.graphics.getHeight()-(int)(input.getY() /*-0*b.getHeight()*/)))
				{
					b.setColor(100, 100, 100, 20);
					while(!input.getButtonStatus());
					application.setScreen(screens.get(i),this);
				}
				else
				{
					b.setColor(200, 0, 200, 20);
				}
			}
			else
			{

			}
			i++;

			if(b.isMouseOverButton(input.getX(), Gdx.graphics.getHeight()-(int)(input.getY())))
			{
				b.setColor(100, 100, 100, 20);
			}
			else
			{
				b.setColor(200, 0, 200, 20);
			}
		}
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void render(float arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}


}
