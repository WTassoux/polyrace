package menu;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class ButtonMenu extends TextButton{
	
	float yMin=0;
	float yMax=0;
	
	public void setRealYCoord(float ymin, float ymax)
	{
		yMin=ymin;
		yMax=ymax;
	}
	public ButtonMenu(String text,Skin skin) {
		super(text, skin);
		// TODO Auto-generated constructor stub
	}
	public ButtonMenu(String text, Skin skin,String s) {
		super(text, skin,s);
		// TODO Auto-generated constructor stub
	}
	public ButtonMenu(String text, TextButtonStyle style) {
		super(text, style);
		// TODO Auto-generated constructor stub
	}
	
	public boolean isMouseOverButton(int x, int y)
	{
		return x>=getX() && x<= (getX()+getWidth()) && y>=(getY()) && y<=(getY()+getHeight());
	}
}
